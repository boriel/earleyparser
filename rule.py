#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:ts=4:et:sw=4:ai:

class Rule(object):
    ''' Defines a Grammar Rule (production), which 
    is something like:
        A --> D B c
    Being A the left symbol, and D B c the left one.
    '''

    def __init__(self, leftSymbol, rightHand = None):
        ''' Initializes the rule. Eg. for the example above,
        use: Rule('A', ('D', 'B', 'c')).
        If the rightHand side must be a tuple or list. If not
        specified, epsilon is used.
        '''
        if rightHand is None:
            rightHand = []

        self.symbol = leftSymbol
        self.right = rightHand
        self.locked = False # Useful for recursive analisys


    def __str__(self):
        ''' String representation
        '''
        right = '€' if not self.right else ' '.join([str(x) for x in self.right])

        return str(self.symbol) + ' --> ' + right


    def __repr__(self):
        return self.__str__()


    def __eq__(self, other):
        return self.symbol == other.symbol and self.right == other.right

