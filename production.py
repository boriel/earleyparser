#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:ts=4:et:sw=4:ai:

from rule import Rule
from action import Action, DefaultAction



class Production(Rule):
    ''' A production is a Rule with semantic actions.
    '''
    def __init__(self, leftSymbol, rightHand = None, action = DefaultAction):
        Rule.__init__(self, leftSymbol, rightHand)
        self.action = action


if __name__ == '__main__':
    P = Production('A')
    print P

