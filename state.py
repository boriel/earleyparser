#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:ts=4:et:sw=4:ai:




class State(object):
    ''' A parser state. It's just a set of marked productions
    '''
    def __init__(self, i):
        self.closureSet = []
        self.i = i # Index

    def __eq__(self, other):
        if len(self.closureSet) != len(other.closureSet):
            return False

        for mp in self.closureSet:
            if mp not in other:
                return False

        return True


    def __len__(self):
        return len(self.closureSet)


    def __getitem__(self, k):
        return self.closureSet[k]


    def addMarkedProduction(self, mp):
        if mp not in self:
            self.closureSet += [mp]

    
    def __contains__(self, mp):
        ''' Returns whether the given Marked Production
        is in this state or not.
        '''
        for markedProd in self.closureSet:
            if mp == markedProd:
                return True

        return False


    def __str__(self):
        result = 'S(%i)\n' % self.i

        if not self.closureSet:
            return result + '<empty>'

        return result + '\n'.join([str(mp) for mp in self.closureSet])




if __name__ == '__main__':
    from markedproduction import MarkedProduction
    from rule import Rule
    a = State(0)
    b = State(0)
    a.addMarkedProduction(MarkedProduction(Rule('A', ['B', 'C']), 0))
    a.addMarkedProduction(MarkedProduction(Rule('A', ['B', 'C']), 0))

    print a
    print b
    print a == b

    b.addMarkedProduction(MarkedProduction(Rule('A', ['B', 'C']), 0))
    print a
    print b
    print a == b

