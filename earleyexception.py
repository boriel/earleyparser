#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:ts=4:et:sw=4:ai:

class EarleyException(Exception):
    ''' Earley parser base exception
    '''
    def __init__(self, msg = 'Earley parser error'):
        self.message = msg

    def __str__(self):
        return self.message

