#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:ts=4:et:sw=4:ai:

from earleyexception import EarleyException
from rule import Rule


class Grammar(object):
    ''' A basic Grammar object.
    This object holds a dictionary. Each key is 
    a LEFT SYMBOL, which leads to a list of rules.
    '''
    def __init__(self):
        ''' Initializes the object with an empty rule set
        '''
        self.rules = {} 
        self.symbols = set([]) # All grammar tokens
        self.nonterminals = set([]) # All non-terminal symbols
        self.terminals = set([]) # Al terminal symbols
        self.nullable = {} # Memoized: is <symbol> nullable?
        self.startSymbol = None


    def contains(self, rule):
        ''' Returns whether the given Rule object
        is already in the grammar or not.
        '''
        rules = self.rules.get(rule.symbol, [])
        return rule in rules


    def isNullable(self, symbol):
        ''' Returns whether a symbol can derivate
        to epsilon
        '''
        result = self.nullable.get(symbol, None)
        if result is not None:
            return result

        result = False
        if symbol in self.nonterminals:
            for rule in self.rules[symbol]:
                if rule.locked:
                    continue

                result = True
                if not rule.right: # Empty rule => Epsilon?
                    break
  
                rule.locked = True # Recursion lock
                for _symbol in rule.right: # Check if all the symbols in the rule righ side are nullable
                    if not self.isNullable(_symbol):
                        result = False
                        break
                rule.locked = False

                if result:
                    break

        self.nullable[symbol] = result
        return result


    def addRule(self, rule):
        ''' Adds the given rule to the grammar set.
        If the rule already exists, an exception is raised.
        This method can be invoked in several ways:

            grammar.addRule(Rule('A', ['B', 'C']))
        '''
        if self.contains(rule):
            raise EarleyException('Duplicated grammar rule: ' + str(rule))

        if self.startSymbol is None: # 1st added symbol is the start symbol
            self.startSymbol = rule.symbol

        if self.rules.get(rule.symbol, None) is None:
            self.rules[rule.symbol] = []

        self.rules[rule.symbol] += [rule]
        self.nonterminals.add(rule.symbol)
        self.symbols.add(rule.symbol)

        for symbol in rule.right:
            self.symbols.add(symbol)
            if symbol not in self.nonterminals:
                self.terminals.add(symbol)

        if rule.symbol in self.terminals: # Remove rule left symbol from terminals
            self.terminals.remove(rule.symbol) 
        

    def __str__(self):
        ''' Returns a string representation of the grammar
        '''
        result = ''

        for symbol in self.rules.keys():
            for rule in self.rules[symbol]:
                result += str(rule) + '\n'

        return result
                


if __name__ == '__main__':
    G = Grammar()
    G.addRule(Rule('A', ['B', 'C', 'd']))
    G.addRule(Rule('A', ['B', 'B']))
    G.addRule(Rule('B', ['A', 'e']))
    G.addRule(Rule('B', []))

    print G
    print G.isNullable('A')
    print G.isNullable('B')
    print G.isNullable('C')

