#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:ts=4:et:sw=4:ai:



class MarkedProduction(object):
    ''' A marked production is a Rule/Production augmented class,
    which contains the dot position and the previous state
    '''
    def __init__(self, rule, state):
        self.rule = rule
        self.pos = 0
        self.state = state


    @property
    def completed(self):
        return self.pos >= len(self.rule.right)


    @property
    def symbol(self):
        return self.rule.symbol


    def forward(self):
        if not self.completed:
            self.pos += 1


    def __str__(self): 
        tmp = [symbol for symbol in self.rule.right]
        tmp = tmp[:self.pos] + ['·'] + tmp[self.pos:]
        return str(self.rule.symbol) + ' --> ' + ' '.join(tmp) + ' (%i)' % self.state


    def __eq__(self, other):
        return self.pos == other.pos and self.state == other.state and self.rule == other.rule


    def clone(self):
        ''' Returns a new instance which is a copy of this one
        '''
        result = MarkedProduction(self.rule, self.state)
        result.pos = self.pos
        return result


    @property
    def nextcat(self):
        ''' Returns current instance marked (expected next) symbol or none 
        if completed.
        '''
        return None if self.completed else self.rule.right[self.pos]




if __name__ == '__main__':
    from rule import Rule

    mp = MarkedProduction(Rule('A'), 0)
    print mp
    mp = MarkedProduction(Rule('A', ['B', 'C', 'd']), 0)
    mp2 = MarkedProduction(Rule('A', ['B', 'C', 'd']), 0)
    print mp
    print mp2
    print mp == mp2

    while not mp.completed:
        print mp
        mp.forward()

    print mp


