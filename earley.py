#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:ts=4:et:sw=4:ai:

from parser import Parser
from grammar import Grammar
from state import State
from markedproduction import MarkedProduction
from production import Production


START = '<START>'
EOINPUT = '<EOI>'
DEBUG = False


class EarleyParser(Parser):
    ''' A simple Earley parser
    '''
    def __init__(self):
        ''' A simple earley parser. Use addProduction to
        add productions to the parser Grammar.
        '''
        self.grammar = Grammar()


    @property
    def currentState(self):
        ''' Returns the State object for the current state
        '''
        if len(self.states) <= self.i:
            self.states += [State(self.i)]

        return self.states[self.i]


    @property
    def nextState(self):
        ''' Returns the State object for the next state
        '''
        S = self.currentState # Ensures the current state is already created

        if len(self.states) <= self.i + 1:
            self.states += [State(self.i + 1)]

        return self.states[self.i + 1]


    def addProduction(self, prod):
        self.grammar.addRule(prod)
    

    def start(self):
        self.i = 0 # state counter
        self.states = [] # List of states processed
        self.lookAhead = None

        self.addProduction(Production(START, [self.grammar.startSymbol]))
        for prod in self.grammar.rules[START]:
            self.currentState.addMarkedProduction(MarkedProduction(prod, self.i))


    def parse(self, inputTokens = None):
        ''' Parses the grammar
        '''
        self.start()

        if inputTokens is None:
            inputTokens = []

        inputTokens += [EOINPUT]

        for token in inputTokens:
            self.lookAhead = token
            S = self.currentState

            i = 0
            while i < len(S):
                mp = S[i] # Take the maked production of S[j][i]
                i += 1

                if mp.completed:
                    self.complete(mp)
                elif mp.nextcat in self.grammar.nonterminals:
                    self.predict(mp)
                else:
                    self.scan(mp)

            if token != EOINPUT:
                self.next()

        if DEBUG:
            print self.currentState


    def next(self):
        ''' Jumps to next state
        '''
        if DEBUG:
            print self.currentState

        self.i += 1
            

    def predict(self, mp):
        ''' Tries to add rules whose left symbol is just after the dot
        e.g. A -> .B .... ; add all B -> ... rules.
        '''
        symbol = mp.nextcat

        if symbol in self.grammar.nonterminals: 
            for prod in self.grammar.rules[symbol]:
                self.currentState.addMarkedProduction(MarkedProduction(prod, self.i))

        
    def scan(self, mp):
        ''' Checks for current state productions if any can be added
        to the next state.
        '''
        if mp.nextcat == self.lookAhead and mp.nextcat in self.grammar.terminals:
            newMP = mp.clone()
            newMP.forward()
            self.nextState.addMarkedProduction(newMP)
                    
            
    def complete(self, mp):
        ''' Complete rules with the marked production at the end
        '''
        S = self.states[mp.state]
        for mp2 in S:
            if mp2.completed:
                continue

            if mp2.nextcat == mp.symbol:
                newMP = mp2.clone()
                newMP.forward()
                self.currentState.addMarkedProduction(newMP)


    @property
    def accepts(self):
        ''' Returns if the current state if an accept state
        '''
        for mp in self.currentState:
            if mp.symbol == START:
                return True

        return False
            
                    
if __name__ == '__main__':
    DEBUG = True

    E = EarleyParser()
    E.addProduction(Production('S', ['S', '+', 'M']))
    E.addProduction(Production('S', ['M']))
    E.addProduction(Production('M', ['M', '*', 'T']))
    E.addProduction(Production('M', ['T']))
    E.addProduction(Production('T', ['number']))
    E.parse(['number', '+', 'number', '*', 'number'])

    print 'Accepts' if E.accepts else 'Rejects'

    

