#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:ts=4:et:sw=4:ai:


class Action(object):
    ''' A semantic action, that is invoked as a Function.
        Used as a decorator.
    '''
    def __init__(self, f):
        self.action = f

    def __call__(self, *args, **kwargs):
        self.action(*args, **kwargs)


@Action
def DefaultAction(*args, **kwargs):
    ''' Default Action: just do Nothing.
    This can be overriden.
    '''
    return


if __name__ == '__main__':
    @Action
    def __test():
        print "Hello"

    __test()

